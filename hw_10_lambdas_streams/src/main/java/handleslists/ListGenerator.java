package handleslists;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

class ListGenerator {

    private static RandomNumberGenerator generator = new RandomNumberGenerator();

    // use javafacer to generate Integer[]
    private static Integer[] getIntArray(int length, int min, int max){
        Integer[] array = new Integer[length];
        for (int i = 0; i < array.length; i++) {
           array[i] = generator.numberBetween(min, max);
        }
        return array;
    }

    // use Stream.of(Integer[] array) option, does not accept int[]!!!
    static List<Integer> getListStreamOf(int length, int min, int max){
        Integer[] array = getIntArray(length, min, max);
        return  Stream.of(array).collect(Collectors.toList());
    }

    // use iteration on Stream
    static List<Integer> getListWithStep(int seed, int step, int limit){
        return Stream.iterate(seed, n -> n + step).
                limit(limit).
                collect(Collectors.toList());
    }

    // may use forEach(list::add) to add all elements to List
    static List<Integer> getListForEach(int size, int min, int max) {
        List<Integer> list = new ArrayList<>();
        Stream.generate(() -> (int) (min + (Math.random() * (max - min + 1))))
                .limit(size)
                .forEach(list::add);
        return list;
    }

    // may return List itself using collect(Collectors.toList()
    static List<Integer> getListCollect(int size, int min, int max) {
        return new Random().ints(min, max)
                .limit(size)
                .boxed()
                .collect(Collectors.toList());
    }

}
