package stringsapp.ui;

import logers.BaseView;
import stringsapp.services.Service;

import java.util.ArrayList;
import java.util.List;

public class MyView extends BaseView {

    private Service service;
    private List<String> list;

    public MyView() {
        service = new Service();
        list= new ArrayList<>();
    }

    public void display() {

        generateLog(MyView.class.toString());

        String incomingWord;
        String output;

        while (true) {
            reader.showDialogSingleCommand(
                    "Input a text line \n" +
                            "Or press Cancel to stop and get the result");
            incomingWord = reader.getOption();
            if (incomingWord == null) {
                break;
            } else {
                list.add(incomingWord);
                logger.info("List: " + list.toString());
                reader.replyGreen("List: " + list.toString());
            }
        }
        output=  "\nList: " + list.toString()+
                "\nQuantity of unique words: "
                + service.calculateNumberOfUniqueWords(list)+
                "\nSorted list of unique words: "
                + service.sortUniqueWords(list)+
                "\nQuantity of each unique word: "
                + service.calculateNumberOfWords(list)+
                "\nQuantity of each symbol: "
                + service.calculateNumberOfLowerCaseSymbols(list);

        reader.replyPink(output);
        logger.warn(output);
    }

}
