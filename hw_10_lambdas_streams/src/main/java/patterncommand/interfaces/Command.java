package patterncommand.interfaces;

@FunctionalInterface
public interface Command {

    void execute(String msg);
}
