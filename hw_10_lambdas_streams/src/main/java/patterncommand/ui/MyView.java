package patterncommand.ui;

import logers.BaseView;
import patterncommand.interfaces.Showable;
import patterncommand.services.Service;

import java.util.LinkedHashMap;
import java.util.Map;

public class MyView extends BaseView {

    private Service service;
    private Map<Integer, String> menu;
    private Map<Integer, Showable> methodsMenu;
    private String request = "Enter the message";

    public MyView() {
        service = new Service();

        menu = new LinkedHashMap<>();
        menu.put(1, "1 - Lambda based Command");
        menu.put(2, "2 - Method reference based Command");
        menu.put(3, "3 - Anonymous class based Command");
        menu.put(4, "4 - Object based Command");
        menu.put(5, "5 - QUIT");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put(1, this::pressButton1);
        methodsMenu.put(2, this::pressButton2);
        methodsMenu.put(3, this::pressButton3);
        methodsMenu.put(4, this::pressButton4);
    }

    private void pressButton1() {
        reader.showDialogSingleCommand(request+" to Lambda Based Command");
        service.lambdaBasedCommand(reader.getOption()); }

    private void pressButton2() {
        reader.showDialogSingleCommand(request+" to Method Reference Based Command");
        service.methodReferenceBasedCommand(reader.getOption()); }

    private void pressButton3() {
        reader.showDialogSingleCommand(request+" to Anonymous Class Based Command");
        service.anonymousClassBasedCommand(reader.getOption()); }

    private void pressButton4() {
        reader.showDialogSingleCommand(request+" to Object Based Command");
        service.objectBasedCommand(reader.getOption()); }


    private String outputMenu() {
        StringBuilder s = new StringBuilder("Enter the option: \n ");
        for (String str : menu.values()) {
            s.append(str).append("\n");
        }
        return s.toString();
    }

    public void show() {
        reader.request();
        if(reader.getAnswer()==0){
            int option = 0;
            do {
                reader.showDialogAllOptions(outputMenu());
                try {
                    if ( reader.getOption() == null || reader.getOption().equals("5")){
                        break;
                    }else {
                        option = Integer.parseInt(reader.getOption());
                        methodsMenu.get(option).display();
                    }
                } catch (Exception e) {
                    reader.replyYellow();
                }
            } while (option !=5);
        }else {
            System.exit(0);
        }

    }

}
