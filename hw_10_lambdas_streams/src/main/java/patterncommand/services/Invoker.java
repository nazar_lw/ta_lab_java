package patterncommand.services;

import logers.BaseView;

class Invoker extends BaseView {

    void invoke(String msg){
        reader.replyGreen(msg);
    }
}
