package patterncommand.services;

import patterncommand.interfaces.Command;

public class Service {

    private Invoker invoker;

    public Service() {
        invoker = new Invoker();
    }

    public void lambdaBasedCommand(String msg) {

        Command command = s -> invoker.invoke(s);
        command.execute("Lambda based Command: "+msg);
    }

    public void methodReferenceBasedCommand(String msg) {

        Command command = invoker::invoke;
        command.execute("Method reference based Command: " + msg);
    }

    public void anonymousClassBasedCommand(String msg) {

        Command command = new Command() {
            @Override
            public void execute(String s) {
                invoker.invoke(s);
            }
        };
        command.execute("Anonymous class based Command: "+msg);
    }

    public void objectBasedCommand(String msg){

        invoker.invoke("Object based Command: "+msg);
    }
}
