package ui;

import models.Airline;
import models.CargoPropellerPlane;
import models.abstractmodels.Aircraft;
import models.abstractmodels.Cargo;
import models.propulsiontype.Helicoptable;
import org.json.simple.parser.ParseException;
import services.AircraftService;
import services.AirlineService;
import sources.FillDefaultData;
import sources.FillDefaultLists;

import java.io.IOException;
import java.util.Comparator;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

class DemoCases {

    DemoCases() throws Exception {

        FillDefaultData fillDefaultData = new FillDefaultData();
        fillDefaultData.populateDB();
    }


    private static Logger logger = LogManager.getLogger(App.class);
    private FillDefaultLists fillSource = new FillDefaultLists();
    private List<Airline> defAirlines = fillSource.getDefaultAirlines();
    private AirlineService airlineService = new AirlineService();
    private AircraftService aircraftService = new AircraftService();

    private Airline newAirline = new Airline();
    private CargoPropellerPlane newAircraft = new CargoPropellerPlane();

    void printAllAircraftsSortedByAirline() throws Exception {

        List<Aircraft> aircrafts = aircraftService.getAll();

        aircrafts.sort(Comparator.comparing((Aircraft a) -> a.getAirline().getName()));

        aircrafts.forEach(a-> logger.warn(a.toString()));

    }

    void printAircraftsOfAirline() throws IOException, ParseException {

        logger.warn("--------get aircrafts of an Airline #1-----------");

        List<Aircraft> aircraftsByAirline =
                aircraftService.getByAirline(defAirlines.get(1));

        aircraftsByAirline.forEach(aircraft -> logger.warn(aircraft));
    }

    void saveNewAirline() throws Exception {

        logger.warn("---------------save new Airline------------");

        newAirline.setName("New One");

        logger.warn("Output on save newAirline: "+airlineService.saveAirline(newAirline));
    }

    void deleteExistingAirline() throws Exception {

        logger.warn("------------delete existing Airline #1---------");

        logger.warn("Output on delete newAirline: "+airlineService.deleteAirline(defAirlines.get(1)));

        logger.warn("Output on attempt to delete it again: "+airlineService.deleteAirline(defAirlines.get(1)));

        logger.warn("number of aircrafts of a company after delete: "+aircraftService.getByAirline(defAirlines.get(1)).size());

    }

    void addNewAircraft() throws Exception {
        logger.warn("---------------add new Aircraft--------------");

        newAircraft.setMakeAndModel("New Plane");
        newAircraft.setLoadCapacity(200);
        newAircraft.setFuelConsumption(100);
        newAircraft.setNumberOfTanks(8);
        newAircraft.setAirline(newAirline);
        logger.warn("Output on save aircraft: "+aircraftService.saveAircraft(newAircraft));

        logger.warn("Get just saved aircraft from the DB: "+aircraftService.getByAirline(newAirline).get(0));
    }

    void getCargoAircrafts() throws IOException, ParseException {

        logger.warn("------------get cargo aircrafts---------------");

        List<Aircraft> cargo1 = aircraftService.getByLoadType(defAirlines.get(0), String.valueOf(Cargo.class));

        cargo1.forEach(aircraft -> logger.warn(aircraft));
    }

    void getHelicopters() throws IOException, ParseException {

        logger.warn("-----------------get helicopters-----------------");

        List<Aircraft> helicopters = aircraftService.getByPropulsion(defAirlines.get(0), Helicoptable.TYPE);

        helicopters.forEach(aircraft -> logger.warn(aircraft));
    }

    void deleteAircraft() throws Exception {

        logger.warn("---------delete aircraft----------");

        logger.warn("Output on delete aircraft: "+aircraftService.deleteAircraft(newAirline, newAircraft));

        logger.warn("Outputon attempt to delete it again: "+aircraftService.deleteAircraft(newAirline, newAircraft));
    }

    void getTotalPassengerCapacity() throws IOException, ParseException {

        logger.warn("--get total passenger capacity-------");

        logger.warn("total Passenger Capacity: "+aircraftService.totalPassengerCapacity(defAirlines.get(0)));
    }

    void getTotalLoadCapacity() throws IOException, ParseException {

        logger.warn("---get total load capacity-----");

        logger.warn("total Load Capacity: "+aircraftService.totalLoadCapacity(defAirlines.get(2)));
    }

    void sortByFlightRange() throws IOException, ParseException {

        logger.warn("----sort by flight range----");

        List<Aircraft> sortedByFlightRange = aircraftService.sortedByFlightRange(defAirlines.get(0));

        sortedByFlightRange.forEach(a-> logger.warn(a.flightRange()));
    }

    void findByFuelConsumption() throws IOException, ParseException {

        logger.warn("-----find by fuel consumption-----");

        List<Aircraft> aircraftListByConsumption = aircraftService.findByFuelConsumption(defAirlines.get(2), 100);

        aircraftListByConsumption.forEach(a-> {
            logger.warn(a.getFuelConsumption());
            logger.warn(a.toString());
        });
    }

    void getSimilarAircraftsFromDB() throws IOException, ParseException {

        logger.warn("----find aircrafts despite airline, equals ignores airline ----");

        Aircraft aircraftRandom = aircraftService.getByIndex(2);

        logger.warn("---setting a nonexistent Airline before comparing within the list----");
        aircraftRandom.setAirline(new Airline());

        List<Aircraft> allSimilar = aircraftService.getAllSimilarFromDB(aircraftRandom);

        allSimilar.forEach(a-> logger.warn(a.toString()));
    }

}
