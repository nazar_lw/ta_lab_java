package services;

import models.Airline;
import models.abstractmodels.Aircraft;
import models.abstractmodels.Cargo;
import models.abstractmodels.Passenger;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class AircraftService extends AbstarctService{

    @SuppressWarnings("unchecked")
    public List<Aircraft> getAll() throws IOException, ParseException {

        return (List<Aircraft>) aircraftDAO.getAll();
    }

    public List<Aircraft> getByAirline(Airline airline) throws IOException, ParseException {

        return aircraftDAO.getAll(airline);
    }

    @SuppressWarnings("unchecked")
    public String saveAircraft(Aircraft aircraft) throws Exception {

        List<Aircraft> aircrafts = (List<Aircraft>) aircraftDAO.getAll();
        aircrafts.add(aircraft);
        aircraftDAO.saveAll(aircrafts);
        return "Aircraft saved";
    }

    @SuppressWarnings("unchecked")
    public String deleteAircraft(Airline airline, Aircraft aircraft) throws Exception {

        List<Aircraft> aircrafts = (List<Aircraft>) aircraftDAO.getAll();

        String response ="";

        if(aircrafts.stream().noneMatch(a -> a.getMakeAndModel().equals(aircraft.getMakeAndModel()))){
            response= "Such aircraft does not exist";
        }else {
            List<Aircraft> newList  =
                    aircrafts.stream().
                    filter(a->((!a.getMakeAndModel().equals(aircraft.getMakeAndModel()))&&(!a.getAirline().equals(airline)))).
                    collect(Collectors.toList());
            aircraftDAO.saveAll(newList);
            response = "Aircraft deleted";
        }
        return response;
    }

    public List<Aircraft> getByLoadType(Airline airline, String className) throws IOException, ParseException {

        List<Aircraft> aircrafts = aircraftDAO.getAll(airline);
        List<Aircraft> aircraftsSorted = new ArrayList<>();

        if (className.equals(Cargo.class.toString())) {
            aircraftsSorted = aircrafts.stream()
                    .filter(a -> a.getLoadCapacity() > 0)
                    .collect(Collectors.toList());
        }else if (className.equals(Passenger.class.toString())){
            aircraftsSorted = aircrafts.stream()
                    .filter(a -> a.getPassengerCapacity() > 0)
                    .collect(Collectors.toList());
        }
        return aircraftsSorted;
    }

    public List<Aircraft> getByPropulsion(Airline airline, String propulsionType) throws IOException, ParseException {

        List<Aircraft> aircrafts = aircraftDAO.getAll(airline);

        return aircrafts.stream()
                .filter(a -> a.getPropulsion().equals(propulsionType))
                .collect(Collectors.toList());
    }

    public int totalPassengerCapacity(Airline airline) throws IOException, ParseException {

        List<Aircraft> passengerAircrafts =
                this.getByLoadType(airline, String.valueOf(Passenger.class));

        return passengerAircrafts.stream()
                .map(Aircraft::getPassengerCapacity)
                .reduce(0, Integer::sum);
    }

    public int totalLoadCapacity(Airline airline) throws IOException, ParseException {

        List<Aircraft> cargoAircrafts = this.getByLoadType(airline, String.valueOf(Cargo.class));

        return cargoAircrafts.stream()
                .map(Aircraft::getLoadCapacity)
                .reduce(0, Integer::sum);
    }

    public List<Aircraft> sortedByFlightRange(Airline airline) throws IOException, ParseException {

        List<Aircraft> aircrafts = aircraftDAO.getAll(airline);

        return aircrafts.stream()
                .sorted(Comparator.comparingDouble(Aircraft::flightRange))
                .collect(Collectors.toList());
    }

    public List<Aircraft> findByFuelConsumption(Airline airline, int fuelConsumption) throws IOException, ParseException {
        List<Aircraft> aircrafts = aircraftDAO.getAll(airline);

        return aircrafts.stream()
                .filter(a -> a.getFuelConsumption()==fuelConsumption)
                .collect(Collectors.toList());
    }

    public Aircraft getByIndex(int index) throws IOException, ParseException {

        return (Aircraft) aircraftDAO.getAll().get(index);
    }

    @SuppressWarnings("unchecked")
    public List<Aircraft> getAllSimilarFromDB(Aircraft aircraftWithoutAirline) throws IOException, ParseException {

        List<Aircraft> listAllFromDB = (List<Aircraft>) aircraftDAO.getAll();
        return listAllFromDB.stream().filter(a->a.equals(aircraftWithoutAirline)).collect(Collectors.toList());
    }
}
