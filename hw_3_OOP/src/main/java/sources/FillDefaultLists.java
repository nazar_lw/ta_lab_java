package sources;

import models.*;
import models.abstractmodels.Aircraft;

import java.util.Arrays;
import java.util.List;

public class FillDefaultLists {

    public List<Airline> getDefaultAirlines(){

        Airline a1 = new Airline();
        a1.setName("Aerosvit");
        Airline a2 = new Airline();
        a2.setName("MAU");
        Airline a3 = new Airline();
        a3.setName("LOT");

        return Arrays.asList(a1, a2, a3);
    }

    private List<Aircraft> getDefaultAircrafts(){

        CargoHelicopter ch1 = new CargoHelicopter();
        ch1.setMakeAndModel("Apache");
        ch1.setLoadCapacity(10);
        ch1.setFuelConsumption(30);
        ch1.setNumberOfTanks(2);

        CargoHelicopter ch2 = new CargoHelicopter();
        ch2.setMakeAndModel("Sikorsky");
        ch2.setLoadCapacity(15);
        ch2.setFuelConsumption(35);
        ch2.setNumberOfTanks(1);

        PassengerHelicopter ph1 = new PassengerHelicopter();
        ph1.setMakeAndModel("Kamov");
        ph1.setPassengerCapacity(10);
        ph1.setFuelConsumption(15);
        ph1.setNumberOfTanks(2);

        PassengerHelicopter ph2 = new PassengerHelicopter();
        ph2.setMakeAndModel("Bell");
        ph2.setPassengerCapacity(22);
        ph2.setFuelConsumption(45);
        ph2.setNumberOfTanks(3);

        CargoJetPlane cjp1 = new CargoJetPlane();
        cjp1.setMakeAndModel("AN");
        cjp1.setLoadCapacity(100);
        cjp1.setFuelConsumption(60);
        cjp1.setNumberOfTanks(4);

        CargoJetPlane cjp2 = new CargoJetPlane();
        cjp2.setMakeAndModel("Mrija");
        cjp2.setLoadCapacity(120);
        cjp2.setFuelConsumption(70);
        cjp2.setNumberOfTanks(4);

        CargoJetPlane cjp3 = new CargoJetPlane();
        cjp3.setMakeAndModel("Boeng");
        cjp3.setLoadCapacity(90);
        cjp3.setFuelConsumption(50);
        cjp3.setNumberOfTanks(2);

        CargoJetPlane cjp4 = new CargoJetPlane();
        cjp4.setMakeAndModel("Douglass");
        cjp4.setLoadCapacity(55);
        cjp4.setFuelConsumption(30);
        cjp4.setNumberOfTanks(2);

        PassengerJetPlane pjp1 = new PassengerJetPlane();
        pjp1.setMakeAndModel("AN");
        pjp1.setPassengerCapacity(150);
        pjp1.setFuelConsumption(50);
        pjp1.setNumberOfTanks(4);

        PassengerJetPlane pjp2 = new PassengerJetPlane();
        pjp2.setMakeAndModel("TU");
        pjp2.setPassengerCapacity(190);
        pjp2.setFuelConsumption(70);
        pjp2.setNumberOfTanks(6);

        PassengerJetPlane pjp3 = new PassengerJetPlane();
        pjp3.setMakeAndModel("Boeng");
        pjp3.setPassengerCapacity(270);
        pjp3.setFuelConsumption(89);
        pjp3.setNumberOfTanks(6);

        PassengerJetPlane pjp4 = new PassengerJetPlane();
        pjp4.setMakeAndModel("Airbus");
        pjp4.setPassengerCapacity(300);
        pjp4.setFuelConsumption(100);
        pjp4.setNumberOfTanks(8);

        CargoPropellerPlane cpp1 = new CargoPropellerPlane();
        cpp1.setMakeAndModel("AN");
        cpp1.setLoadCapacity(105);
        cpp1.setFuelConsumption(70);
        cpp1.setNumberOfTanks(2);

        CargoPropellerPlane cpp2 = new CargoPropellerPlane();
        cpp2.setMakeAndModel("IL");
        cpp2.setLoadCapacity(50);
        cpp2.setFuelConsumption(30);
        cpp2.setNumberOfTanks(3);

        PassengerPropellerPlane ppp1 = new PassengerPropellerPlane();
        ppp1.setMakeAndModel("AN");
        ppp1.setPassengerCapacity(100);
        ppp1.setFuelConsumption(66);
        ppp1.setNumberOfTanks(2);

        PassengerPropellerPlane ppp2 = new PassengerPropellerPlane();
        ppp2.setMakeAndModel("Douglas");
        ppp2.setPassengerCapacity(170);
        ppp2.setFuelConsumption(100);
        ppp2.setNumberOfTanks(6);

        return Arrays.asList(
                ch1,
                ch2,
                ph1,
                ph2,
                cjp1,
                cjp2,
                cjp3,
                cjp4,
                pjp1,
                pjp2,
                pjp3,
                pjp4,
                cpp1,
                cpp2,
                ppp1,
                ppp2);
    }

    public List<Aircraft> fill(){
        List<Airline> airlines = this.getDefaultAirlines();
        List<Aircraft> aircrafts = this.getDefaultAircrafts();
                aircrafts.get(0).setAirline(airlines.get(0));
                aircrafts.get(1).setAirline(airlines.get(1));
                aircrafts.get(2).setAirline(airlines.get(2));
                aircrafts.get(3).setAirline(airlines.get(0));
                aircrafts.get(4).setAirline(airlines.get(1));
                aircrafts.get(5).setAirline(airlines.get(2));
                aircrafts.get(6).setAirline(airlines.get(0));
                aircrafts.get(7).setAirline(airlines.get(2));
                aircrafts.get(8).setAirline(airlines.get(1));
                aircrafts.get(9).setAirline(airlines.get(0));
                aircrafts.get(10).setAirline(airlines.get(1));
                aircrafts.get(11).setAirline(airlines.get(2));
                aircrafts.get(12).setAirline(airlines.get(0));
                aircrafts.get(13).setAirline(airlines.get(0));
                aircrafts.get(14).setAirline(airlines.get(1));
                aircrafts.get(15).setAirline(airlines.get(2));

        return aircrafts;
    }
}
