package models;

import models.abstractmodels.Passenger;
import models.propulsiontype.Propellerable;

public class PassengerPropellerPlane extends Passenger implements Propellerable {

    public PassengerPropellerPlane() {
        this.setType();
        this.setVolume();
    }

    public void setType() {
        this.setPropulsion(this.TYPE);
    }

    public void setVolume() {
        this.setTankVolume(this.TANK_VOLUME);
    }

    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public String toString() {
        return "PassengerPropellerPlane{} " + super.toString();
    }
}
