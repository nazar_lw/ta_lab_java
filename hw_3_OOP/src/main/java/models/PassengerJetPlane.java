package models;

import models.abstractmodels.Passenger;
import models.propulsiontype.Jetable;

public class PassengerJetPlane extends Passenger implements Jetable {

    public PassengerJetPlane() {
        this.setType();
        this.setVolume();
    }

    public void setType() {
        this.setPropulsion(this.TYPE);
    }

    public void setVolume() {
        this.setTankVolume(this.TANK_VOLUME);
    }

    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public String toString() {
        return "PassengerJetPlane{} " + super.toString();
    }
}


