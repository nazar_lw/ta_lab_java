package models.abstractmodels;

public abstract class Cargo extends Aircraft {

    protected Cargo() {
    }

    @Override
    public double flightRange() {
        return super.flightRange();
    }

    public void setLoadCapacity(int loadCapacity){
        super.loadCapacity = loadCapacity;
    }

}
