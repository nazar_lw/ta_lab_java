package models.abstractmodels;

public abstract class Passenger extends Aircraft {

    protected Passenger() {
    }

    @Override
    public double flightRange() {
        return super.flightRange();
    }

    public void setPassengerCapacity(int passengerCapacity){
        super.passengerCapacity = passengerCapacity;
    }

}
