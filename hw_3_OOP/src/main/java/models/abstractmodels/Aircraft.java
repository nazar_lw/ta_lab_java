package models.abstractmodels;

// in general this class might have been abstract
// but in this case it would not be possible to serialize it from Gson
// in the statement: gson.fromJson(jsonObject.toString(), Aircraft.class
// in AircraftDAO.getAll();

import models.Airline;

public class Aircraft {

    private String makeAndModel;
    private double fuelConsumption;
    private  int numberOfTanks;
    private  int tankVolume;
    private String propulsion;
    int passengerCapacity;
    int loadCapacity;
    private Airline airline = new Airline();

    Aircraft() {
    }

    public double flightRange(){
        return tankVolume*numberOfTanks/fuelConsumption;
    }


    public String getMakeAndModel() {
        return makeAndModel;
    }

    public void setMakeAndModel(String makeAndModel) {
        this.makeAndModel = makeAndModel;
    }

    public double getFuelConsumption() {
        return fuelConsumption;
    }

    public void setFuelConsumption(double fuelConsumption) {
        this.fuelConsumption = fuelConsumption;
    }

    public void setNumberOfTanks(int numberOfTanks) {
        this.numberOfTanks = numberOfTanks;
    }

    public String getPropulsion() {
        return propulsion;
    }

    protected void setTankVolume(int tankVolume) {
        this.tankVolume = tankVolume;
    }

    protected void setPropulsion(String propulsion) {
        this.propulsion = propulsion;
    }

    public int getPassengerCapacity() {
        return passengerCapacity;
    }

    public int getLoadCapacity() {
        return loadCapacity;
    }

    public Airline getAirline() {
        return airline;
    }

    public void setAirline(Airline airline) {
        this.airline = airline;
    }

    // airline field is not included to equals @ hashCode to enable
    // comparison of aircrafts from separate airlines within one DB
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Aircraft aircraft = (Aircraft) o;

        if (Double.compare(aircraft.fuelConsumption, fuelConsumption) != 0) return false;
        if (numberOfTanks != aircraft.numberOfTanks) return false;
        if (tankVolume != aircraft.tankVolume) return false;
        if (passengerCapacity != aircraft.passengerCapacity) return false;
        if (loadCapacity != aircraft.loadCapacity) return false;
        if (makeAndModel != null ? !makeAndModel.equals(aircraft.makeAndModel) : aircraft.makeAndModel != null)
            return false;
        return propulsion != null ? propulsion.equals(aircraft.propulsion) : aircraft.propulsion == null;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = makeAndModel != null ? makeAndModel.hashCode() : 0;
        temp = Double.doubleToLongBits(fuelConsumption);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + numberOfTanks;
        result = 31 * result + tankVolume;
        result = 31 * result + (propulsion != null ? propulsion.hashCode() : 0);
        result = 31 * result + passengerCapacity;
        result = 31 * result + loadCapacity;
        return result;
    }

    @Override
    public String toString() {
        return "Aircraft{" +
                "makeAndModel='" + makeAndModel + '\'' +
                ", fuelConsumption=" + fuelConsumption +
                ", numberOfTanks=" + numberOfTanks +
                ", tankVolume=" + tankVolume +
                ", propulsion='" + propulsion + '\'' +
                ", passengerCapacity=" + passengerCapacity +
                ", loadCapacity=" + loadCapacity +
                ", airline=" + airline +
                '}';
    }
}
