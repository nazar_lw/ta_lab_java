package models.propulsiontype;

public interface InternalCombustable {
    void setType();
    void setVolume();
}
