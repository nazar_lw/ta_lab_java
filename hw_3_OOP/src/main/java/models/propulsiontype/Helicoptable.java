package models.propulsiontype;

public interface Helicoptable extends InternalCombustable {

    String TYPE = "Helicopter Engine";
    int TANK_VOLUME= 2000;

}
