package dao;

import com.google.gson.Gson;

import java.io.File;
import java.util.List;

public interface AbstractDAO {

     Gson gson = new Gson();

    String PATH_AIRLINES_FILE = System.getProperty("user.dir") +
            File.separator +"airlinesList.json";

    String PATH_AIRCRAFT_FILE = System.getProperty("user.dir") +
            File.separator +"aircraftsList.json";


     @SuppressWarnings("unchecked")
     void saveAll(List<?> list) throws Exception;

     @SuppressWarnings("unchecked")
     List<?> getAll()throws Exception;

}
