package trafficlightsmenu;


public enum TrafficLights {

    RED("Stop, do not go!"),
    YELLOW("Wait, get ready."),
    GREEN("Your turn. Go!");

    private String message;

    TrafficLights(String msg) {
        message = msg;
    }

    public String getMessage() {
        return message;
    }
}