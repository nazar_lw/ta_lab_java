package trafficlightsmenu;

@FunctionalInterface
public interface Showable {

    void display();
}
