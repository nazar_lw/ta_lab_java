package trafficlightsmenu;

import javax.swing.*;
import java.awt.*;

class Reader{

    int answer;
    String option;

    void request(){

        UIManager.put("Panel.background", Color.cyan);
        answer = JOptionPane.showConfirmDialog (
                null, "Do you want to test the Traffic Lights?");
    }

    void showDialog(String request){

        UIManager.put("OptionPane.messageForeground", Color.blue);
        UIManager.put("Panel.background", Color.pink);
        option = JOptionPane.showInputDialog(request);
    }

    void replyR(String reply){

        UIManager.put("OptionPane.messageForeground", Color.DARK_GRAY);
        UIManager.put("Panel.background", Color.red);
        JOptionPane.showMessageDialog(null, reply);
   }

    void replyY(String reply){

        UIManager.put("OptionPane.messageForeground", Color.DARK_GRAY);
        UIManager.put("Panel.background", Color.yellow);
        JOptionPane.showMessageDialog(null, reply);
    }

    void replyG(String reply){

        UIManager.put("OptionPane.messageForeground", Color.DARK_GRAY);
        UIManager.put("Panel.background", Color.green);
        JOptionPane.showMessageDialog(null, reply);
    }


    void replyW(){

        UIManager.put("OptionPane.messageForeground", Color.YELLOW);
        UIManager.put("Panel.background", Color.RED);
        JOptionPane.showMessageDialog(
                null, "Wrong Input, numbers from 1 to 4 only!");
    }
}

