package customizedmap.experimentalLinkedMap;

import customizedmap.Model;
import logers.BaseLoggerClass;

import java.util.Arrays;
import java.util.List;

public class MainClassLinkedMap extends BaseLoggerClass {

    public static void main(String[] args) {

        generateLog(MainClassLinkedMap.class.toString());

        logger.debug("Create a new CustomizedLinkedMap<Integer, String>");
        CustomizedLinkedMap<Integer, String> map = new CustomizedLinkedMap<>();

        logger.info("Check isEmpty(): "+map.isEmpty());
        logger.info("check size(): "+map.size());

        logger.info("Put some keys & values to an empty map");
        logger.warn(map.put(1, "ASD"));
        logger.warn(map.put(2, "QWE"));
        logger.warn(map.put(3, "CVB"));
        logger.warn(map.put(4, "HJK"));
        logger.info("Check get(3): "+map.get(3));
        logger.info("Check nonexistent get(5): "+map.get(5));
        logger.info("Check isEmpty(): "+map.isEmpty());
        logger.info("check size(): "+map.size());
        logger.info("Put a value to an existing key: "+map.put(2, "Something else"));
        logger.info("Get a new value: "+map.get(2));
        logger.info("Check size(): "+map.size());
        logger.warn("Print all the map: "+map.print());
        logger.info("Clear all data");
        map.clear();
        logger.info("Check size(): "+map.size());
        logger.warn("Print all the map  after clear(): "+map.print());

        logger.debug("Create a new CustomizedLinkedMap<String, List<Integer>>");
        CustomizedLinkedMap<String, List<Integer>> map1 = new CustomizedLinkedMap<>();

        logger.info("Create a few lists");
        List<Integer> list1 = Arrays.asList(1, 2, 3);
        List<Integer> list2 = Arrays.asList(4, 5, 6);
        logger.warn("Put lists to an empty map: "+map1.put("A", list1));
        logger.warn("Put lists to an empty map: "+map1.put("B", list2));
        logger.info("Print all the map: "+map1.print());
        map1.clear();
        logger.info("Print all the map after clear() : "+map1.print());

        logger.debug("Create a new CustomizedLinkedMap<String, List<Integer>>");
        CustomizedLinkedMap<Integer, List<Model>> map2=
                new CustomizedLinkedMap<>();

        logger.info("Create a few lists");
        List<Model> models1 = Arrays.asList(
                new Model(7777, "Petro"),
                new Model(2222, "Ivan"));
        List<Model> models2 = Arrays.asList(
                new Model(333, "Maria"),
                new Model(987, "Galia"));
        logger.warn("Put lists to an empty map: "+map2.put(44, models1));
        logger.warn("Put lists to an empty map: "+map2.put(66, models2));
        logger.info("Print all the map: "+map2.print());
        logger.info("Get a model instance from the map");
        Model model = map2.get(44).get(1);
        logger.info("Print model: "+model.toString());

    }

}
