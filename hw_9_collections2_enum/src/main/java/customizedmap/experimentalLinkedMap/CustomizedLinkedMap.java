package customizedmap.experimentalLinkedMap;

@SuppressWarnings("unchecked")
public class CustomizedLinkedMap<K extends Comparable, V>  {

    private int size = 0;
    private Node<K, V> root;

    public int size() {
        return size;
    }

    public boolean isEmpty(){
        return size <= 0;
    }


    public V put(K key, V value) {
        Node<K, V> main;
        Node<K, V> tmp;
        if (root == null) { // create new Map
            root = new Node<>(key, value);
        } else {
            tmp = root;
            int resultOfComparator;
            do {
                main = tmp;
                resultOfComparator = key.compareTo(tmp.getKey());
               // set all nodes in order by keys
                if (resultOfComparator < 0) {
                    tmp = tmp.getPrevious();
                } else if (resultOfComparator > 0) {
                    tmp = tmp.getNext();
                } else {
                    V existingValue = tmp.getValue();
                    tmp.setValue(value);
                    return existingValue;
                }
            } while (tmp != null);

            if (resultOfComparator < 0) {
                main.setPrevious(new Node<>(key, value));
            } else {
                main.setNext(new Node<>(key, value));
            }
        }
        // add iterators to a class field
        size++;
        return null;
    }

    public String print(){
        Node<K, V> node = root;
        StringBuilder s= new StringBuilder();
        while (node != null) {
            s.append("{key: ").append(node.getKey()).
                    append(", value: ").append(node.getValue().toString()).
                    append("}");
            node = node.getNext();
        }
        return s.toString();
    }

    public V get(K key) {
        Comparable<? super K> compareKeys = (Comparable<? super K>) key;
        Node<K, V> node = root;
        while (node != null) {
            // run on the all nodes and compare keys
            int resultOfComparator = compareKeys.compareTo(node.getKey());
            if (resultOfComparator < 0) {
                node = node.getPrevious();
            } else if (resultOfComparator > 0) {
                node = node.getNext();
            } else {
                return node.getValue();
            }
        }
        return null;
    }

    public void clear() {
        root = null;
        size = 0;
    }

}

