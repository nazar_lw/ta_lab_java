package customizedmap.experimentalLinkedMap;

@SuppressWarnings("unchecked")
class Node<K extends Comparable, V> implements Comparable<Node<K, V>>{

    private K key;
    private V value;

    private Node<K, V> previous;
    private Node<K, V> next;

    Node(K key, V value) {
        this.key = key;
        this.value = value;
    }

    Node<K, V> getPrevious() {
        return previous;
    }

    Node<K, V> getNext() {
        return next;
    }

    void setPrevious(Node<K, V> previous) {
        this.previous = previous;
    }

    void setNext(Node<K, V> next) {
        this.next = next;
    }

    K getKey() {
        return key;
    }

    V getValue() {
        return value;
    }

    void setValue(V value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Node<?, ?> node = (Node<?, ?>) o;

        if (!key.equals(node.key)) return false;
        if (!value.equals(node.value)) return false;
        if (!previous.equals(node.previous)) return false;
        return next.equals(node.next);
    }

    @Override
    public int hashCode() {
        int result = key.hashCode();
        result = 31 * result + value.hashCode();
        result = 31 * result + previous.hashCode();
        result = 31 * result + next.hashCode();
        return result;
    }

    @Override
    public int compareTo(Node<K, V> o) {
        return this.getKey().compareTo(o.getKey());
    }
}
