package theory.datastorage;

//https://www.baeldung.com/java-stack-heap

public class Driver {

    public static void main(String[] args) {

//        primitive value of integer id will be stored directly in stack memory
        int id = 23;

//        reference variable of String argument personName which
//        will point to the actual string from string pool in heap memory
        String pName = "Jon";

//        reference variable p of type Person will also be created in stack memory
//        which will point to the actual object in the heap
        Person p = null;

//        for the newly created object p of type Person, all instance variables
//        will be stored in heap memory.
        p = new Person(id, pName);
    }
}
