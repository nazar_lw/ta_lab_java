package garden.gardenCustomizedException;

public class ColorException extends Exception {
    public ColorException( Throwable cause) {
        super("Wrong color input", cause);
    }
}
