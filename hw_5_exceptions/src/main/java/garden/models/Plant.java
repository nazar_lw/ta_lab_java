package garden.models;

import garden.constants.Color;
import garden.constants.Type;
import garden.gardenCustomizedException.ColorException;
import garden.gardenCustomizedException.TypeException;

public class Plant {

    private int size;
    private Color color;
    private Type type;

    public Plant() {
    }

    public Plant(int size, String color, String type) throws ColorException, TypeException {
        this.size = size;

        try {
            this.color = Color.valueOf(color);
        }catch (IllegalArgumentException e){
            throw new ColorException( e);
        }

        try {
            this.type = Type.valueOf(type);
        }catch (IllegalArgumentException e){
            throw new TypeException( e);
        }
    }

    @Override
    public String toString() {
        return "Plant{" +
                "size=" + size +
                ", color=" + color +
                ", type=" + type +
                '}';
    }
}
