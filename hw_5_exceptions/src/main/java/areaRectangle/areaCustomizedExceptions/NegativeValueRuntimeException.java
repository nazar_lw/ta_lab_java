package areaRectangle.areaCustomizedExceptions;

public class NegativeValueRuntimeException extends RuntimeException{
    public NegativeValueRuntimeException(String message) {
        super(message);
    }
}
