package arrays.game.ui;

import arrays.game.services.GameLogic;

// TODO: I would rename this class as well to something like GameEngine, GameRunner, Game .....
public class GameEngine {

    private GameLogic service;

    public GameEngine() {
        this.service = new GameLogic();
    }

    public void runDemo() {

        service.introduceHero();
        service.getDoors(20); // 10 by task
        service.demise();
        service.victory();
        service.safeOrderOfDoors();
    }
}
