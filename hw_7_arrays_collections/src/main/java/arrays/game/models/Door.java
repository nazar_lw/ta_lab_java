package arrays.game.models;

import java.util.Random;
//TODO: !!!!!!!!!!!!!!!!!!!!! DOOR can't extend RandomGenerator because it is not a RandomGenerator
//!!!!!!!!!!!!!!!!! We have a lot of another options to reuse functionality of RandomGenerator
public class Door {

    public boolean isMonster;
    public Monster monster;
    public Artifact artifact;
    private static Random rand = new Random();

    public Door() {
        isMonster = rand.nextBoolean();
        if (isMonster) {
            monster = new Monster();
        } else {
            artifact = new Artifact();
        }
    }

    @Override
    public String toString() {
        return "Door{" +
                "isMonster=" + isMonster +
                ", monster=" + monster +
                ", artifact=" + artifact +
                "} " ;
    }
}
