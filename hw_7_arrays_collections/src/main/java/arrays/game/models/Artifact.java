package arrays.game.models;

import java.util.Random;

public class Artifact {

    private static final int MIN_POWER = 10;
    private static final int MAX_POWER = 80;

    private int power;
    private static Random rand = new Random();

    Artifact() {
        power = rand.nextInt((MAX_POWER - MIN_POWER) + 1) + MIN_POWER;
    }

    @Override
    public String toString() {
        return "Artifact{" +
                "power=" + power +
                "} ";
    }
}
