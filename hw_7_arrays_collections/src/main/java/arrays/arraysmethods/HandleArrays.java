package arrays.arraysmethods;

import static arrays.arraysmethods.Utils.*;

class HandleArrays {

    int[] deleteNonUniqueElements(int[] array) {
        int[] newArray;
        boolean[] nonUniqueElements = markNonUnique(array);
        //generate new array from incoming marked by flag in boolean array
        newArray = deleteElementsByFlagsArray(array, nonUniqueElements,false);
        return bubbleSort(newArray);
    }

    int[] joinArraysElementsExistInBothArrays(int[] arr1, int[] arr2) {

        int[] array = new int[0];
        arr1 = deleteNonUniqueElements(arr1);
        arr2 = deleteNonUniqueElements(arr2);

        boolean[] nonUnique1 = findNonUniqueElementsFromArrays(arr1, arr2);
        boolean[] nonUnique2 = findNonUniqueElementsFromArrays(arr2, arr1);

        arr1 = deleteElementsByFlagsArray(arr1, nonUnique1,true);
        arr2 = deleteElementsByFlagsArray(arr2, nonUnique2,true);

        array = joinArrays(array, arr1);
        array = joinArrays(array, arr2);

        array = deleteNonUniqueElements(array);
        return array;
    }

    int[] joinArraysElementsExistInOneArray(int[] arr1, int[] arr2) {

        int[] arr = new int[0];
        arr1 = deleteNonUniqueElements(arr1);
        arr2 = deleteNonUniqueElements(arr2);

        boolean[] nonUnique1 = findNonUniqueElementsFromArrays(arr1, arr2);
        boolean[] nonUnique2 = findNonUniqueElementsFromArrays(arr2, arr1);

        arr1 = deleteElementsByFlagsArray(arr1, nonUnique1,false);
        arr2 = deleteElementsByFlagsArray(arr2, nonUnique2,false);

        arr = joinArrays(arr, arr1);
        arr = joinArrays(arr, arr2);

        arr = deleteNonUniqueElements(arr);
        return arr;
    }

    int[] deleteMoreThanTwoNonUniqueElements(int[] array) {

        boolean[] nonUniqueElements = markNonUnique(array);
        int[] arrayWithoutNonUnique = deleteNonUniqueElements(array);

        //allocate all non unique elements from incoming array
        int[] arrayOfNonUniqueElements = new int[0];
        for(int i=0; i<nonUniqueElements.length; i++){
            if(nonUniqueElements[i]){
                arrayOfNonUniqueElements = addElement(arrayOfNonUniqueElements, array[i]);
            }
        }
        // delete all nonUnique elements from arrayOfNonUniqueElements
        int[] cleanedArr = deleteNonUniqueElements(arrayOfNonUniqueElements);
        // join arrays so that there will be no more than 2 non unique elements
        return bubbleSort(joinArrays(arrayWithoutNonUnique, cleanedArr));
    }

    int[] deleteGroupNonUniqueElementsOnRow(int[] array) {
        int[] newArray = new int[0];
        int nonUniqueElement;
        for (int i = 0; i < array.length - 1; i++) {
            nonUniqueElement = array[i];
            while (nonUniqueElement == array[i + 1]) {
                if (i < array.length - 2) {
                    i++;
                } else break;
            }
            newArray = joinArrays(newArray, new int[]{nonUniqueElement});
        }
        if (array[array.length - 2] != array[array.length - 1]) {
            newArray = joinArrays(newArray, new int[]{array[array.length - 1]});
        }
        return newArray;
    }

}
