package arrays.arraysmethods;

import logers.BaseLoggerClass;

import java.util.Arrays;

import static arrays.arraysmethods.Utils.printArray;



//TODO: this is not a good case for inheritance  MainClassHandleArrays is not a logger but it has a logger
public class MainClassHandleArrays extends BaseLoggerClass {
// TODO: p s f Logger
    public static void main(String[] args) {

        generateLog(MainClassHandleArrays.class.toString());

        logger.info("Create HandleArrays instance to test methods");
        HandleArrays h = new HandleArrays();
        logger.info("Create array to test removing non-unique elements");
        int[] arr = {5, 4, 10, 4, 3, 2, 1, 1, 6, 7, 5, 7, 8, 9, 4};
        logger.trace("Before removing: "+printArray(arr));
        logger.info("Delete non-unique elements: "+
                printArray(h.deleteNonUniqueElements(arr)));
        //TODO: use cntrl + alt + L for autoformating or other ways to apply autoformat before push (empty spaces between + and text etc)
        logger.info("Create 2 arrays to test join of them");
        int[] arr1 = {1, 2, 3, 4, 5, 6, 88, 77};
        logger.trace("Before removing: "+printArray(arr1));
        int[] arr2 = {0, 3, 99, 88, 77};
        logger.trace("Before removing: "+printArray(arr2));
        logger.info("Elements exist in Both Arrays: "+
                printArray(h.joinArraysElementsExistInBothArrays(arr1, arr2)));
        logger.info("Elements exist one array: "+
                printArray(h.joinArraysElementsExistInOneArray(arr1, arr2)));
        // TODO: Regarding logging - it's ok for HW but if we imagine that it is production code logging would be better to place inside methods and on top abstraction level you should avoid so many logging         logger.info("Create array to test removing non unique elements if there are more than 2 of them");
        int[] arr3 = {44, 1, 3, 1, 3, 44, 5, 6, 3, 44, 1, 2, 44, 3};
        logger.trace("Before removing: "+printArray(arr3));
        logger.info("Non unique elements detected and reduced to not more than 2 of each: "+
                printArray(h.deleteMoreThanTwoNonUniqueElements(arr3)));

        logger.info("Create array to test removing equal elements in a row with leaving one ");
        int[] arr4 = {1, 2, 2, 2, 2, 3, 4, 5, 6, 6, 6, 7, 6, 2};
        logger.trace("Before removing: "+printArray(arr4));
        logger.info("Groups of elements detected and reduced to 1 element in each group: "
                +printArray(h.deleteGroupNonUniqueElementsOnRow(arr4)));
    }
}
