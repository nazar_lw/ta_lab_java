package arrays.arraysmethods;


public class Utils {

    static int[] bubbleSort(int[] arr){

        for(int i = arr.length - 1; i > 0; --i) {
            for(int j = 0; j < i; ++j) {
                if(arr[j] > arr[j + 1]) {
                    int tmp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = tmp;
                }
            }
        } return arr;

    }

    static int[] joinArrays(int[] arr1, int[] arr2){

        int[] arrConcatenated = new int[arr1.length+arr2.length];

        for (int i = 0; i < arr1.length; i++) {
            arrConcatenated[i] = arr1[i];
        }
        int j = arr1.length;

        for (int i = 0; i < arr2.length; i++) {
            arrConcatenated[j] = arr2[i];
            j++;
        }
        return arrConcatenated;
    }

    public static int[] addElement(int[] arr, int element){
        int[] newArr = {element};
        return joinArrays(arr, newArr);
    }

    static boolean[] markNonUnique(int[] array){
        boolean[] nonUniqueElements = new boolean[array.length];
        int counter = 0;
        for (int i = 0; i < array.length; i++) {
            if (!nonUniqueElements[i]) {
                for (int j = i; j < array.length; j++) {
                    if (array[i] == array[j]) {
                        counter++;
                        if (counter > 1) {
                            nonUniqueElements[j] = true;
                        }
                    }
                }
                counter = 0;
            }
        }
        return nonUniqueElements;
    }

    //mark elements from first array that are non unique to second array
    static boolean[] findNonUniqueElementsFromArrays(int[] array, int[] nonUniqueElementsArr) {
        boolean[] nonUniqueElements = new boolean[array.length];
        for (int i = 0; i < array.length; i++) {
            for (int nonUnique : nonUniqueElementsArr) {
                if (array[i] == nonUnique) {
                    nonUniqueElements[i] = true;
                }
            }
        }
        return nonUniqueElements;
    }

    static int[] deleteElementsByFlagsArray(int[] array, boolean[] flags, boolean flag){
        int[] newArray = new int[0];
        for (int i = 0; i < flags.length; i++) {
            // add marked by flag elements to new array
            if (flags[i] == flag) {
                newArray = joinArrays(newArray, new int[]{array[i]});
            }
        }
        return newArray;
    }

    public static int[] removeElement(int[] arr, int numberOfElement){

        if(numberOfElement>(arr.length-1)){
            return arr;
        }else {
            int[] arrBeforeElement = new int[numberOfElement];
            int[] arrAfterElement = new int[arr.length-numberOfElement-1];
            for (int i = 0; i < arrBeforeElement.length; i++) {
                arrBeforeElement[i] = arr[i];
            }
            for (int i = 0; i < arrAfterElement.length; i++) {
                arrAfterElement[i] = arr[numberOfElement+i+1];
            }
            return joinArrays(arrBeforeElement, arrAfterElement);
        }
    }

    //TODO: Good job !!! but use such apporach for leanrning purpose only for example it could replaced with ArrayUtils.toString(a) - from apache common library that you already import
    public static String printArray(int[] arr){
        String s="[";
        if(arr.length==0){
            s="[]";
        }else {
            for (int i = 0; i < arr.length; i++) {
                if(i==arr.length-1){
                    s+=arr[i]+"]";
                }else {
                    s+=arr[i]+", ";
                }
            }
        }
        return s;

    }

}
