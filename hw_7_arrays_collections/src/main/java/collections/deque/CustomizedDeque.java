package collections.deque;

import java.util.*;

public class CustomizedDeque {

    private Object[] array;
    // TODO: what about additional constructors where you could set initial capacity ??
    CustomizedDeque() {
        array = new Object[0];
    } //TODO: you could start from 10 ? 100 ? why zero

    public void addFirst(Object o) {
        if (array.length == 0) {
            array = new Object[1];
            array[0] = o;
        } else {
            Object[] newArray = new Object[array.length + 1]; // TODO: in case of maaaany operations it could cause memory leaks because you will recreate very often it better to double array size or resize for some index
            newArray[0] = o;
            for (int i = 1; i < newArray.length; i++) {
                newArray[i] = array[i - 1];
            }
            array = newArray;
        }
    }

    public void addLast(Object o) {
        if (array.length == 0) {
            array = new Object[1];
            array[0] = o;
        } else {
            Object[] newItems = new Object[array.length + 1];
            newItems[newItems.length - 1] = o;
            for (int i = 0; i < newItems.length - 1; i++) {
                newItems[i] = array[i];
            }
            array = newItems;
        }
    }

    public boolean offerFirst(Object o) {
        try {
            addFirst(o);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean offerLast(Object o) {
        try {
            addLast(o);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public Object removeFirst() {
        Object object = array[0];
        Object[] newArray = new Object[array.length - 1];
        try {
            for (int i = 0; i < newArray.length; i++) {
                newArray[i] = array[i + 1];
            }
            array = newArray;
            return object;
        } catch (Exception e) {
            throw new NoSuchElementException();
        }
    }

    public Object removeLast() {
        Object object = array[array.length - 1];
        Object[] newArray = new Object[array.length - 1];
        try {
            for (int i = 0; i < newArray.length; i++) {
                newArray[i] = array[i];
            }
            array = newArray;
            return object;
        } catch (Exception e) {
            throw new NoSuchElementException();
        }
    }

    public Object pollFirst() {
        try {
            return removeFirst();
        } catch (Exception e) {
            return null;
        }
    }

    public Object pollLast() {
        try {
            return removeLast();
        } catch (Exception e) {
            return null;
        }
    }

    public Object getFirst() {
        try {
            return array[0]; // element[0] does not exist
        } catch (Exception e) {
            throw new ArrayIndexOutOfBoundsException();
        }
    }

    public Object getLast() {
        try {
            return array[array.length - 1]; // element[array.length - 1] does not exist
        } catch (Exception e) {
            throw new ArrayIndexOutOfBoundsException();
        }
    }

    public Object peekFirst() {
        try {
            return getFirst();
        } catch (Exception e) {
            return null;
        }
    }

    public Object peekLast() {
        try {
            return getLast();
        } catch (Exception e) {
            return null;
        }
    }
    @Override
    public String toString() {
        return "CustomizedDeque{" +
                "array=" + Arrays.toString(array) +
                '}';
    }
}
