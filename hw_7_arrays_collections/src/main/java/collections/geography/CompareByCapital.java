package collections.geography;

public class CompareByCapital implements Comparable<CompareByCapital> {

    private Country country;

    @Override
    public String toString() {
        return "CompareByCapital{" +
                "country=" + country +
                '}';
    }

    CompareByCapital(Country country) {
        this.country = country;
    }


    @Override
    public int compareTo(CompareByCapital o) {
        return this.country.getCapital().compareTo(o.country.getCapital());
    }
}
