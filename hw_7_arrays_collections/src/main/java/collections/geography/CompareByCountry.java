package collections.geography;

public class CompareByCountry implements Comparable<CompareByCountry>{

    private Country country;

    CompareByCountry(Country country) {
        this.country = country;
    }

    @Override
    public String toString() {
        return "CompareByCountry{" +
                "country=" + country +
                '}';
    }

    @Override
    public int compareTo(CompareByCountry o) {
        return this.country.getCountryName().compareTo(o.country.getCountryName());
    }
}
