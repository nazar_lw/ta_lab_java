package collections.geography;

public class Country {
//TODO: I would mark them as final
    private final String countryName;
    private final String capital;

    Country(String country, String capital) {
        this.countryName = country;
        this.capital = capital;
    }

    String getCountryName() {
        return countryName;
    }

    String getCapital() {
        return capital;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Country country = (Country) o;

        if (countryName != null ? !countryName.equals(country.countryName) : country.countryName != null) return false;
        return capital != null ? capital.equals(country.capital) : country.capital == null;
    }

    @Override
    public int hashCode() {
        int result = countryName != null ? countryName.hashCode() : 0;
        result = 31 * result + (capital != null ? capital.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Country{" +
                "countryName='" + countryName + '\'' +
                ", capital='" + capital + '\'' +
                '}';
    }

    //TODO: dont forget to override equals and hashcode for classes it is even more important than to string

}
