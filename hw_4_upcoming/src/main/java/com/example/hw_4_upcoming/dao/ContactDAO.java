package com.example.hw_4_upcoming.dao;

import com.example.hw_4_upcoming.models.Contact;
import org.springframework.data.jpa.repository.JpaRepository;


public interface ContactDAO extends JpaRepository<Contact, Integer> {

}