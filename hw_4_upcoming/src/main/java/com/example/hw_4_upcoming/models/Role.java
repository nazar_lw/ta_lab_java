package com.example.hw_4_upcoming.models;

public enum Role {
    ROLE_ADMIN,
    ROLE_USER
}