package com.example.hw_4_upcoming.services;

import com.example.hw_4_upcoming.dao.ContactDAO;
import com.example.hw_4_upcoming.models.Contact;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ContactService {
    private final ContactDAO contactDAO;

    @Autowired
    public ContactService(
            ContactDAO contactDAO) {
        this.contactDAO = contactDAO;

    }

    public void save(Contact contact){
        if(contact!=null){
            contactDAO.save(contact);
        }
    }

    public void deleteAll(List<Contact> contacts){
        contactDAO.deleteAll(contacts);
    }

    public List<Contact> findAll(){
        return contactDAO.findAll();
    }

    public Contact getOne(int id){

        return contactDAO.getOne(id);
    }

}
