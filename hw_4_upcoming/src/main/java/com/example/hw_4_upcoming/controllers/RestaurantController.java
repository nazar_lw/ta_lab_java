package com.example.hw_4_upcoming.controllers;

import com.example.hw_4_upcoming.dao.UserDAO;
import com.example.hw_4_upcoming.models.Restaurant;
import com.example.hw_4_upcoming.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.regex.Pattern;

@RestController
public class RestaurantController {

    private final PasswordEncoder passwordEncoder;
    private final UserService userService;
    private final UserDAO userDAO;

    @Autowired
    public RestaurantController(PasswordEncoder passwordEncoder, UserService userService, UserDAO userDAO) {
        this.passwordEncoder = passwordEncoder;
        this.userService = userService;
        this.userDAO = userDAO;
    }

    @PostMapping("/saveRestaurant")
    public String saveRestaurant(@RequestBody Restaurant restaurant) {

        System.out.println(restaurant.toString());

        String statement;

        Pattern pattern =
                Pattern.compile("\\+\\d{2,3}\\(\\d{2,3}\\)\\d{3}\\-?\\d{2}\\-?\\d{2}");

        if((restaurant.getUsername().length()<4)||(restaurant.getPassword().length()<4)){
            statement = "Wrong input- the length of both login and password must be at least 4 characters";
        } else if (!pattern.matcher(restaurant.getPhoneNumber()).find()) {
            statement= "WRONG FORMAT of the PHONE NUMBER!!";
        } else {
            restaurant.setPassword(passwordEncoder.encode(restaurant.getPassword()));
            try {
                userService.save(restaurant);
            } catch (Exception e) {
                System.out.println(e);
                return restaurant.getUsername() + " has not been saved, user with such login already exists ";
            }
            statement= userDAO.findByUsername(restaurant.getUsername()).getUsername() + " has been saved successfully";
        }
        return statement;
    }
}
