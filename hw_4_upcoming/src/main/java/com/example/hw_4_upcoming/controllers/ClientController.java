package com.example.hw_4_upcoming.controllers;

import com.example.hw_4_upcoming.dao.UserDAO;
import com.example.hw_4_upcoming.models.Client;
import com.example.hw_4_upcoming.services.impl.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ClientController {

    private final PasswordEncoder passwordEncoder;

    private final UserDAO userDAO;
    private final UserServiceImpl userServiceImpl;

    @Autowired
    public ClientController(PasswordEncoder passwordEncoder, UserDAO userDAO, UserServiceImpl userServiceImpl) {
        this.passwordEncoder = passwordEncoder;

        this.userDAO = userDAO;
        this.userServiceImpl = userServiceImpl;
    }

    @PostMapping("/saveClient")
    public String saveRestaurant(@RequestBody Client client) {

        String statement;
        if((client.getUsername().length()<4)||(client.getPassword().length()<4)){
            statement = "Wrong input- the length of both login and password must be at least 4 characters";
        }else {
            client.setPassword(passwordEncoder.encode(client.getPassword()));
            try {
                userServiceImpl.save(client);
                statement = userDAO.findByUsername(client.getUsername()).getUsername()+" has been saved successfully";
            }catch (Exception e){
                System.out.println(e);
                return client.getUsername()+" has not been saved, user with such login already exists";
            }
        }

        return statement;
    }
}
