package logers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class BaseLoggerClass {

     protected static Logger logger =
            LogManager.getLogger(BaseLoggerClass.class);

    protected static void generateLog(String s){
        logger.debug("=> START LOGGING from: "+s+" <=");
    }

}
