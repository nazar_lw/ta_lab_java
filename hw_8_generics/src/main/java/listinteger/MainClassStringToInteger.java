package listinteger;

import logers.BaseLoggerClass;

import java.util.*;

public class MainClassStringToInteger extends BaseLoggerClass {

    public static void main(String[] args) {


        generateLog(MainClassStringToInteger.class.toString());

        logger.info("Can not add String to List<Integer>");
        List<Integer> list = new ArrayList<>();
//        list.add(Integer.valueOf("SOMETHING"));
//        System.out.println(list.get(0));

        logger.info("Can add bots String and Integer to RAW List");
        List list1 = new ArrayList();
        list1.add(120);
        list1.add("SOMETHING");
        logger.info("In this case list1 stores both types");
        logger.warn(list1.get(0) + " class: "+list1.get(0).getClass().toString());
        logger.warn(list1.get(1) + " class: "+list1.get(1).getClass().toString());
        logger.info("Try to find the class of list");
        logger.warn("list1 instanceof List: "+(list1 instanceof List));
        logger.info("Can not find it to List<Integer>");
//        logger.warn("list1 instanceof List: "+(list1 instanceof List<List>));
        logger.info("Add raw list1 with both Integer and String to a List<Integer>");
        list.addAll(list1);
        logger.warn("Check if List<Integer> list contains string elements: "+list.toString());
        logger.info("Attempt to get String value from list.get(1) produces compilation error:");
        //String s = list.get(1);
        logger.info("Try to get Integer value from list.get(1): ");
        try{
            Integer i = list.get(1);
        }catch (Exception e){
            logger.error(e.getMessage());
        }

    }
}
