package customizedpriorityqueue;

import droidcarrier.models.Aircraft;
import droidcarrier.models.Droid;
import droidcarrier.models.HelicopterDroid;
import droidcarrier.models.SupersonicDroid;
import logers.BaseLoggerClass;

public class MainClassPriorityQueue extends BaseLoggerClass {

    public static void main(String[] args) {

        generateLog(MainClassPriorityQueue.class.toString());

        logger.info("--Create instances of models--");

        Aircraft aircraft1 = new Aircraft("Aircraft-1");
        Droid droid1 = new Droid("Droid-1");
        HelicopterDroid helicopterDroid1 =
                new HelicopterDroid("HelicopterDroid-1");
        SupersonicDroid supersonicDroid1 =
                new SupersonicDroid("SupersonicDroid-1");

        logger.debug("--Create an instance of RAW PriorityQueue WITHOUT declared type--");
        CustomizedPriorityQueue queueRaw = new CustomizedPriorityQueue();

        //TODO such method invocation inside logger is not acceptable outside learning projects,
        //TODO: some loggers could be lazy and if you put some logic invocation inside logger and level does not match in some case method could be skipped
        logger.info("Try to add strings");
        logger.info("queueRaw.add(\"SOMETHING\"): "+queueRaw.add("SOMETHING"));
        logger.info("queueRaw.contains(\"SOMETHING\"): "+queueRaw.contains("SOMETHING"));
        logger.info("queueRaw.add(\"SOMETHING\"): "+queueRaw.add("SOMETHING"));
        logger.info("queueRaw.contains(\"SOMETHING\"): "+queueRaw.contains("SOMETHING"));
        logger.warn("Can NOT add anything - generic array inside queueRaw is parametrized to String");
        logger.warn("queueRaw.add(aircraft1): "+queueRaw.add(aircraft1));
        logger.info("Poll first element");
        logger.info("queueRaw.peek(): "+queueRaw.peek());
        logger.info("queueRaw.poll(): "+queueRaw.poll());
        logger.info("Poll first element again");
        logger.info("queueRaw.poll(): "+queueRaw.poll());
        logger.info("Try if contains String");
        logger.warn("queueRaw.contains(\"SOMETHING\"): "+queueRaw.contains("SOMETHING"));
        logger.info("Try to add an Aircraft");
        logger.info("queueRaw.add(aircraft1): "+queueRaw.add(aircraft1));
        logger.info("queueRaw.contains(aircraft1): "+queueRaw.contains(aircraft1));
        logger.warn("Again can NOT add anything of another type");
        logger.warn("queueRaw.add(droid1): "+queueRaw.add(droid1));
        logger.info("Poll Aircraft");
        logger.info("queueRaw.poll(): "+queueRaw.poll());
        logger.info("Try to add a Droid");
        logger.info("queueRaw.add(droid1): "+queueRaw.add(droid1));
        logger.info("queueRaw.contains(droid1): "+queueRaw.contains(droid1));
        logger.info("Try to add a SupersonicDroid");
        logger.warn("queueRaw.add(supersonicDroid1): "+queueRaw.add(supersonicDroid1));
        logger.info("It works - it is a descendant of Droid");

        logger.debug("--Create an instance of PriorityQueue WITH declared type--");
        CustomizedPriorityQueue<Droid> queueDeclared = new CustomizedPriorityQueue();

        logger.warn("Can not add an Aircraft because it does not implements Comparable");
//        queueDeclared.add(aircraft1);
        logger.warn("Can not add an String because it is not type of Droid");
//        queueDeclared.add("SOMETHING");
        logger.info("Try to add the rest of models");
        logger.info("queueDeclared.add(droid1): "+queueDeclared.add(droid1));
        logger.info("queueDeclared.add(supersonicDroid1): "+queueDeclared.add(supersonicDroid1));
        logger.info("queueDeclared.add(helicopterDroid1): "+queueDeclared.add(helicopterDroid1));
        logger.info("queueDeclared.contains(helicopterDroid1): "+queueDeclared.contains(helicopterDroid1));
        logger.info("Peek for the first element");
        logger.info("queueDeclared.peek(): "+queueDeclared.peek());
        logger.info("Print queueRaw and make sure that it is sorted by names");
        logger.info("queueDeclared.toString(): "+queueDeclared.toString());
        logger.warn("Pool the first element");
        logger.warn("queueDeclared.poll(): "+queueDeclared.poll());
        logger.info("Print queueRaw after poll");
        logger.info("queueDeclared.toString(): "+queueDeclared.toString());
        logger.warn("Try to remove the element that was first");
        logger.warn("queueDeclared.remove(droid1): "+queueDeclared.remove(droid1));
        logger.info("Print queueRaw again");
        logger.info("queueDeclared.toString(): "+queueDeclared.toString());
    }

}
