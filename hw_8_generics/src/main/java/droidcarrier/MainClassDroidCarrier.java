package droidcarrier;

import droidcarrier.models.*;
import logers.BaseLoggerClass;

import java.util.ArrayList;
import java.util.List;

public class MainClassDroidCarrier extends BaseLoggerClass {

    public static void main(String[] args) {

        generateLog(MainClassDroidCarrier.class.toString());

        logger.info("Create instances of models");
        Aircraft aircraft1 = new Aircraft("Aircraft-1");
        Droid droid1 = new Droid("Droid-1");
        HelicopterDroid helicopterDroid1 =
                new HelicopterDroid("HelicopterDroid-1");
        SupersonicDroid supersonicDroid1 =
                new SupersonicDroid("SupersonicDroid-1");

        logger.debug("Create an instance of RAW Ship - WITHOUT declared type");
        Ship shipRAW = new Ship();

        logger.info("Try to load instances to shipRAW");
        shipRAW.loadDroid(aircraft1);
        logger.info("shipRAW.unloadDroid(): "+shipRAW.unloadDroid());
        shipRAW.loadDroid(droid1);
        logger.info("shipRAW.unloadDroid(): "+shipRAW.unloadDroid());
        shipRAW.loadDroid(helicopterDroid1);
        logger.info("shipRAW.unloadDroid(): "+shipRAW.unloadDroid());
        shipRAW.loadDroid(supersonicDroid1);
        logger.info("shipRAW.unloadDroid(): "+shipRAW.unloadDroid());

        logger.info("Create an instance of Ship WITH declared type");
        Ship<Droid> shipDeclaredType = new Ship<>();

        logger.info("Try to load instances to shipRAW");
        logger.warn("Can NOT load aircraft1 since it does not implement Comparable");
//        shipRAW.loadDroid(aircraft1); // DOES not compile
        shipDeclaredType.loadDroid(droid1);
        logger.info("shipRAW.unloadDroid(): "+ shipDeclaredType.unloadDroid());
        shipDeclaredType.loadDroid(supersonicDroid1);
        logger.info("shipRAW.unloadDroid(): "+ shipDeclaredType.unloadDroid());
        shipDeclaredType.loadDroid(helicopterDroid1);
        logger.info("shipRAW.unloadDroid(): "+ shipDeclaredType.unloadDroid());

        logger.debug("Create lists of instances");
        List<Aircraft> aircrafts = new ArrayList<>();
        List<Droid> droids = new ArrayList<>();
        List<SupersonicDroid> supersonicDroids = new ArrayList<>();
        List<HelicopterDroid> helicopterDroids = new ArrayList<>();

        logger.info("It is possible to load any kind of data - ship is RAW, type is not declared");
        shipRAW.loadAsuperAircraft(aircrafts, aircraft1);
        shipRAW.loadAsuperAircraft(droids, aircraft1);
        shipRAW.loadAsuperAircraft(supersonicDroids, helicopterDroid1);

        logger.info("Can not load aircrafts - type of ship is declared and Aircraft does not implement Comparable");
//        shipRAW.loadAsuperDroid(aircrafts, aircraft1);
        logger.info("All else are possible to load ");
        shipDeclaredType.loadAsuperDroid(droids, droid1);
        shipDeclaredType.loadAsuperDroid(aircrafts, droid1);
        shipDeclaredType.loadAsuperDroid(droids, supersonicDroid1);
        logger.info("Can not load List<SupersonicDroid> because the method accepts only Droid and higher of it, not lower");
//        shipDeclaredType.loadAsuperDroid(supersonicDroids, supersonicDroid1);

        logger.info("Can not load any List except List<Aircraft> - it accepts only Aircraft and higher");
//        shipDeclaredType.loadAsuperAircraft(droids, aircraft1);
//        shipDeclaredType.loadAsuperAircraft(droids, droid1);
//        shipDeclaredType.loadAsuperAircraft(supersonicDroids, supersonicDroid1);

        logger.info("It is possible to load any descendants of Aircraft");
        shipDeclaredType.loadAsuperAircraft(aircrafts, aircraft1);
        shipDeclaredType.loadAsuperAircraft(aircrafts, droid1);
        shipDeclaredType.loadAsuperAircraft(aircrafts, helicopterDroid1);

    }
}
