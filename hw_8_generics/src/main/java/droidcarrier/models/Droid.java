package droidcarrier.models;

public class Droid extends Aircraft implements Comparable<Droid>{

    public Droid(String name) {
        super(name);
    }


    @Override
    public int compareTo(Droid o) {
        return this.getName().compareTo(o.getName());
    }

    @Override
    public String toString() {
        return "Droid{} " + super.toString();
    }

//TODO; equals hashcode
}
