package droidcarrier.models;

import java.util.List;

public class Ship <T extends Aircraft & Comparable<T>>{

    private T item;

    public void loadDroid(T droid){
        this.item = droid;
    }

    public T unloadDroid(){
        return this.item;
    }


    // here List accepts Droid and its ancestors only!
    public  <A extends Droid> void loadAsuperDroid(List<? super Droid> list, A droid){
        list.add(droid);
    }

    // here List accepts Aircraft and its ancestors only!
    public  <A extends Aircraft> void loadAsuperAircraft(List<? super Aircraft> list, A droid){
        list.add(droid);
    }

}
