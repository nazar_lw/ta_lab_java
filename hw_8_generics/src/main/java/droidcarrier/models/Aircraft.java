package droidcarrier.models;

public class Aircraft {
// TODO: private ? autoformat ?
    private String name;

    public Aircraft(String name) {
        this.name = name;
    }

    String getName() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Aircraft aircraft = (Aircraft) o;

        return name != null ? name.equals(aircraft.name) : aircraft.name == null;
    }

    @Override
    public int hashCode() {
        return name != null ? name.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "Aircraft{" +
                "name=" + name +
                '}';
    }
}
