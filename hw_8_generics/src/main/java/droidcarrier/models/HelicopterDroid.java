package droidcarrier.models;

public class HelicopterDroid extends Droid {


    public HelicopterDroid(String name) {
        super(name);
    }

    @Override
    public String toString() {
        return "HelicopterDroid{} " + super.toString();
    }
}
