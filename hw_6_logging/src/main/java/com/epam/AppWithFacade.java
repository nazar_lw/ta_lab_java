package com.epam;

//import org.apache.logers.log4j.LogManager;
//import org.apache.logers.log4j.Logger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

class AppWithFacade {

//    private static Logger logger2 = LogManager.getLogger(AppWithFacade.class);
//
//    static void GenerateLog() {
//        logger2.trace("This is a trace message !!!");
//        logger2.debug("This is a debug message !!!");
//        logger2.info("This is an info message !!!");
//        logger2.warn("This is a warn message !!!");
//        logger2.error("This is an error message !!!");
//        logger2.fatal("This is a fatal message !!!");
//    }

    // using slf4j
    // add jar to classpath- https://www.youtube.com/watch?v=NZaH4tjwMYg
    // https://jar-download.com/artifacts/org.slf4j/slf4j-simple/1.7.28/source-code

    private static Logger logger2 = LoggerFactory.getLogger(AppWithFacade.class);

    static void GenerateLog() {
        logger2.trace("This is a trace message !!!");
        logger2.debug("This is a debug message !!!");
        logger2.info("This is an info message !!!");
        logger2.warn("This is a warn message !!!");
        logger2.error("This is an error message !!!");
    }

}
